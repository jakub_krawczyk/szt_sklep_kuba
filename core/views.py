from django.shortcuts import render, get_object_or_404
from .models import Product, Contact

def index(request):
  return render(request, 'core/index.html')

def products(request):
  products = Product.objects.order_by('-pub_date')
  return render(request, 'core/products.html', {
    'list': products
  })

def contact(request):
  try:
    contact = Contact.objects.all()[:1].get()
    return render(request, 'core/contact.html', {
      'contact': contact
    })
  except ObjectDoesNotExist:
    return render(request, 'core/contact.html')
  