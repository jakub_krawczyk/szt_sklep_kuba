from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20181211_1000'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='price',
            name='id',
        ),
        migrations.RemoveField(
            model_name='product',
            name='price',
        ),
        migrations.AddField(
            model_name='price',
            name='product',
            field=models.OneToOneField(default=10, on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='core.Product'),
            preserve_default=False,
        ),
    ]
