from django.contrib import admin

from .models import Contact, Product, Price

class PriceInline(admin.TabularInline):
  model = Price
  extra = 0

class ProductAdmin(admin.ModelAdmin):
  list_display = ['name']
  inlines = [PriceInline]

class ContactAdmin(admin.ModelAdmin):
  def has_add_permission(self, request, obj=None):
        return (Contact.objects.all().count() == 0)
  def has_delete_permission(self, request, obj=None):
        return (Contact.objects.all().count() == 1)


admin.site.register(Product, ProductAdmin)
admin.site.register(Contact, ContactAdmin)
