from django.db import models
from datetime import date

class Contact(models.Model):
  telephone = models.CharField(max_length=12)
  email = models.CharField(max_length=100)
  address = models.CharField(max_length=200)

  def __str__(self):
    return self.address

class Product(models.Model):
  name = models.CharField(max_length=200)
  description = models.CharField(max_length=600, blank = True)
  image = models.ImageField(blank = True)
  pub_date = models.DateField(default=date.today)
  amount = models.IntegerField()

  def __str__(self):
    return self.name

class Price(models.Model):
  product = models.OneToOneField(Product, on_delete=models.DO_NOTHING, primary_key=True)
  amount = models.DecimalField(max_digits=6, decimal_places=2)
  currency = models.CharField(max_length=200)
  
  def __str__(self):
    return str(self.amount) + ' ' + self.currency

